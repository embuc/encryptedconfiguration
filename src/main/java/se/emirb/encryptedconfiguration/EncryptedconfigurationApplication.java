package se.emirb.encryptedconfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

/**
 * The Class EncryptedconfigurationApplication.
 *
 * @author Emir Bucalovic (embuc)
 * @since 2 Mar 2022
 */
@SpringBootApplication
@EnableEncryptableProperties
@PropertySource(value = "classpath:application.properties")
public class EncryptedconfigurationApplication {

	@SuppressWarnings("javadoc")
	public static void main(String[] args) {
		SpringApplication.run(EncryptedconfigurationApplication.class, args);
	}

}
