/*
 * © Copyright Tactel AB 2022
 * All copyrights in this software are created and owned
 * by Tactel AB. This software, or related intellectual
 * property, may under no circumstances be used, distributed
 * or modified without written authorization from Tactel AB.
 * This copyright notice may not be removed or modified and
 * shall be displayed in all materials that include the
 * software or portions of such.
 *
 * Created on 2 Mar 2022.
 */
package se.emirb.encryptedconfiguration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Class HelloController.
 *
 * @author Emir Bucalovic (embuc)
 * @since 2 Mar 2022
 */
@RestController
@SuppressWarnings("javadoc")
public class HelloController {

	@Value("${secret.property}")
	String secret;

	@GetMapping
	public String hello() {
		return "Hello world " + secret;
	}
}
